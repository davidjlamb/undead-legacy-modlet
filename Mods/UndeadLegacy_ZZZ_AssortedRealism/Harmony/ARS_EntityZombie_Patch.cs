using DMT;
using HarmonyLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Runtime.CompilerServices;
using System.IO;
using UnityEngine;

public class ARS_EntityZombie_Patch {
    [HarmonyPatch(typeof(EntityAlive), "DamageEntity")]
    public class Patch
    {
        [HarmonyReversePatch]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public static int DamageEntity(EntityAlive instance, DamageSource _damageSource, int _strength, bool _criticalHit, float impulseScale) {
            throw new NotImplementedException("It's a stub");
        }
    }

    [HarmonyPatch(typeof(EntityZombie))]
    [HarmonyPatch("DamageEntity")]
    public class ARS_EntityZombie_DamageEntity {
        static bool Prefix(EntityZombie __instance, ref DamageSource _damageSource, ref int _strength) {
            if (_strength > 999)
                return true;
            EnumBodyPartHit bodyPart = _damageSource.GetEntityDamageBodyPart(__instance);
            if (bodyPart == EnumBodyPartHit.Head) {
                _damageSource.DamageMultiplier = 1f;
                _damageSource.DismemberChance = 0.8f;
            } else {
                _damageSource.DamageMultiplier = 0.2f;
                _strength = 1;
            }
            return false;
        }
        static int Postfix(int __result, EntityZombie __instance, DamageSource _damageSource, int _strength, bool _criticalHit, float impulseScale) {
            return Patch.DamageEntity(__instance, _damageSource, _strength, _criticalHit, impulseScale);
        }
    }
}