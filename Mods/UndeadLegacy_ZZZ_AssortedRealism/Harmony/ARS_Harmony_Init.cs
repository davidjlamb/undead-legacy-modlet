using DMT;
using HarmonyLib;
using System.Reflection;
using UnityEngine;

public class ARS_Harmony_Init : IHarmony
{
  public void Start()
  {
    Debug.Log((object) (" Loading Patch: " + this.GetType().ToString()));
    Application.SetStackTraceLogType(UnityEngine.LogType.Log, StackTraceLogType.None);
    Application.SetStackTraceLogType(UnityEngine.LogType.Warning, StackTraceLogType.None);
    new Harmony(this.GetType().ToString()).PatchAll(Assembly.GetExecutingAssembly());
  }
}
